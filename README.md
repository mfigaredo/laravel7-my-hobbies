
## My Hobbies

**Laravel 7.0** Udemy Course practice project.

https://www.udemy.com/course/laravel-7-for-beginners-practical-course

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
