<?php

namespace App\Http\Controllers;

use App\Hobby;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;

class HobbyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        // $hobbies = Hobby::all();
        // $hobbies = Hobby::paginate(10);
        $hobbies = Hobby::orderBy('created_at', 'DESC')->paginate(10);


        // return $hobbies;
        // dd($hobbies);
        // dd($hobbies->toArray()[0]);
        // return view('hobby.index', compact('hobbies'));
        // return view('hobby.index')->with([
        //     'hobbies' => $hobbies,
        // ]);
        return view('hobby.index', [
            'hobbies' => $hobbies,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        //
        return view('hobby.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required|min:5',
            'image' => 'mimes:jpeg,bmp,png,gif,jpg',
        ]);


        $hobby = new Hobby([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => auth()->id(),
        ]);
        $hobby->save();

        if($request->image) {
            $this->saveImages($request->image, $hobby->id);
        }

        $request->session()->flash('message_success','The hobby <b>' . $hobby->name . '</b> was created.');
        return $this->index();
    }
    // public function store(Hobby $hobby) {
    //     dd(request());
    //     dd($hobby);
    //     $hobby->save();
    //     return $this->index();
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Hobby $hobby)
    {
        $allTags = Tag::all();
        $usedTags = $hobby->tags;
        $availableTags = $allTags->diff($usedTags);
        return view('hobby.show')->with([
            'hobby' => $hobby,
            'usedTags' => $usedTags,
            'availableTags' => $availableTags,
//            'message_success' => Session::get('message_success'),
//            'message_warning' => Session::get('message_warning'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Hobby $hobby)
    {
        // dd($hobby);
        return view('hobby.edit')->with([
            'hobby' => $hobby,
//            'message_success' => Session::get('message_success'),
//            'message_warning' => Session::get('message_warning'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hobby $hobby)
    {
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'required|min:5',
            'image' => 'mimes:jpeg,bmp,png,gif,jpg',
        ]);

        if($request->image) {
            $this->saveImages($request->image, $hobby->id);
        }

        $hobby->update([
            'name' => $request['name'],
            'description' => $request['description'],
        ]);
        $request->session()->flash('message_success', 'The hobby <b>' . $hobby->name . '</b> was updated.');
        return $this->index();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Hobby $hobby
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Hobby $hobby)
    {
        $oldName = $hobby->name;
        $hobby->delete();
        request()->session()->flash('message_success', 'The hobby <b>' . $oldName . '</b> was deleted.');
        return $this->index();
    }

    public function saveImages($image_input, $hobby_id)
    {
        $image = Image::make($image_input);
        if( $image->width() > $image->height() ) {
            // landscape
            $image->widen(1200)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_large.jpg')
                ->widen(60)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_thumb.jpg')
                ->widen(400)->pixelate(12)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_pixelated.jpg');


        } else {
            // portrait
            $image->heighten(900)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_large.jpg')
                ->heighten(60)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_thumb.jpg')
                ->heighten(400)->pixelate(12)
                ->save(public_path() . '/img/hobbies/' . $hobby_id . '_pixelated.jpg');

        }
    }

    public function deleteImages($hobby_id)
    {
        $types = ['large', 'thumb', 'pixelated'];
        foreach($types as $type) {

            if(file_exists(public_path() . '/img/hobbies/' . $hobby_id . '_' . $type .'.jpg') ) {
                unlink(public_path() . '/img/hobbies/' . $hobby_id . '_' . $type . '.jpg');
            }
        }
        request()->session()->flash('message_success','The image was deleted');
        return back();
    }
}
