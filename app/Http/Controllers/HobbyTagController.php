<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Hobby;
use Illuminate\Http\Request;

class HobbyTagController extends Controller
{
    public function getFilteredHobbies($tag_id)
    {
//        die('hobbies filtered for tag #' . $tag_id);
        $tag = new Tag();
        $hobbies = $tag::findOrFail($tag_id)->filteredHobbies()->paginate(10);

        $filter = $tag::find($tag_id);
//        dd($filter);
        return view('hobby.index', compact('hobbies', 'filter'));
    }

    public function attachTag($hobby_id, $tag_id)
    {
        $hobby = Hobby::findOrFail($hobby_id);
        $tag = Tag::findOrFail($tag_id);
        $hobby->tags()->attach($tag_id);
        return back()->with([
            'message_success' => 'The tag <b>' . $tag->name . '</b> was added.',
        ]);
    }

    public function detachTag($hobby_id, $tag_id)
    {
        $hobby = Hobby::findOrFail($hobby_id);
        $tag = Tag::findOrFail($tag_id);
        $hobby->tags()->detach($tag_id);
        return back()->with([
            'message_success' => 'The tag <b>' . $tag->name . '</b> was removed.',
        ]);

    }
}
