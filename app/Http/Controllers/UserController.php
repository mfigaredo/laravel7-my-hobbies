<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $hobbies = $user->hobbies()->latest()->paginate(10);
        return view('user.show')->with(compact('user','hobbies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(User $user)
    {
//        dd($user);
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|min:5',
            'motto' => 'required|min:5',
            'about_me' => 'min:5',
            'image' => 'mimes:jpeg,bmp,png,gif,jpg',
        ]);

        if($request->image) {
            $this->saveImages($request->image, $user->id);
        }

        $user->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'motto' => $request['motto'],
            'about_me' => $request['about_me'],
        ]);
        $request->session()->flash('message_success', 'The user <b>' . $user->name . '</b> was updated.');
        return redirect(route('user.show', ['user' => $user->id ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function saveImages($image_input, $user_id)
    {
        $image = Image::make($image_input);
        if( $image->width() > $image->height() ) {
            // landscape
            $image->widen(500)
                ->save(public_path() . '/img/users/' . $user_id . '_large.jpg')
                ->widen(60)
                ->save(public_path() . '/img/users/' . $user_id . '_thumb.jpg')
                ->widen(300)->pixelate(8)
                ->save(public_path() . '/img/users/' . $user_id . '_pixelated.jpg');


        } else {
            // portrait
            $image->heighten(500)
                ->save(public_path() . '/img/users/' . $user_id . '_large.jpg')
                ->heighten(60)
                ->save(public_path() . '/img/users/' . $user_id . '_thumb.jpg')
                ->heighten(300)->pixelate(8)
                ->save(public_path() . '/img/users/' . $user_id . '_pixelated.jpg');

        }
    }

    public function deleteImages($user_id)
    {
        $types = ['large', 'thumb', 'pixelated'];
        foreach($types as $type) {

            if(file_exists(public_path() . '/img/users/' . $user_id . '_' . $type .'.jpg') ) {
                unlink(public_path() . '/img/users/' . $user_id . '_' . $type . '.jpg');
            }
        }
        request()->session()->flash('message_success','The image was deleted');
        return back();
    }
}
