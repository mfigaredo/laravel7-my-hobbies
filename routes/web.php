<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// var_dump($_SERVER);
// dd(Route);
// die();
// die('okok');
Route::get('/', function () {
    return view('starting_page');
});

Route::get('/info', function () {
    return view('info');
});

// Route::get('test/{name}/{age}', 'HobbyController@index');
Route::resource('hobby', 'HobbyController');
Route::resource('tag', 'TagController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("check-md", "HomeController@checkMD")->middleware("checkType");

// MultiUploader
Route::get('/uploader', function () {
    return view('multi-uploader');
});
Route::post('/upload', function () {
    if ( request()->has( 'files' ) ) {
        $num_files = 0;
        foreach (request('files') as $file) {
            $num_files += $file->storeAs('files', $file->getClientOriginalName(), 'public') === false ? 0 : 1;
        }
        return ['uploaded' => $num_files, 'status' => $num_files > 0 ? 'success' : 'none'];
    }
});

Route::resource('user', 'UserController');
Route::get('/hobby/tag/{tag_id}', 'HobbyTagController@getFilteredHobbies')->name('hobby_tag');

// Attach / Detach Tags to Hobbies
Route::get('/hobby/{hobby_id}/tag/{tag_id}/attach', 'HobbyTagController@attachTag');
Route::get('/hobby/{hobby_id}/tag/{tag_id}/detach', 'HobbyTagController@detachTag');

// Delete images of hobby
Route::get('/delete-images/hobby/{hobby_id}', 'HobbyController@deleteImages');
// Delete images of user
Route::get('/delete-images/user/{user_id}', 'UserController@deleteImages');
