@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">STARTING PAGE</div>

                <div class="card-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum tempora ad quisquam amet nobis enim, corrupti dolore error libero neque saepe maxime quo voluptatibus accusantium ipsa! Illo, dolorum! Obcaecati nam pariatur error ex! Rerum iusto temporibus quaerat aperiam molestiae excepturi, voluptates aut dignissimos tenetur quo labore nostrum quibusdam, veniam illo dolor praesentium corrupti ex quod maxime reprehenderit iure minus. Iure maxime adipisci quibusdam enim, veritatis ipsam! Voluptatem perferendis, voluptates error illo non voluptas accusamus, dolore quaerat eos esse dicta corrupti modi natus tenetur officia possimus animi harum minus! Distinctio laborum explicabo suscipit vitae quos. Praesentium, distinctio. Fugiat nam illum, maxime, cumque facilis recusandae repellat consectetur corrupti aspernatur expedita itaque iure perspiciatis maiores at aperiam temporibus magni sapiente officiis. Quo, voluptatum.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
