@extends('layouts.app')

@section('page_title', 'Hobby Detail')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Hobby Detail</div>

                <div class="card-body">
                    <h4>{{ $hobby-> name }}</h4>
					<p>{{ $hobby->description }}</p>
                    @if($hobby->tags->count() > 0)
                    <b>Used Tags:</b> (Click to remove)
                    <p>
                        @foreach($hobby->tags as $tag)
                            <a href="/hobby/{{ $hobby->id }}/tag/{{ $tag->id }}/detach"><span class="badge badge-{{ $tag->style }}">{{ $tag->name }}</span></a>
                        @endforeach
                    </p>
                    @endif
                    @if($availableTags->count() > 0)
                    <b>Available Tags:</b> (Click to assign)
                    <p>
                        @foreach($availableTags as $tag)
                            <a href="/hobby/{{ $hobby->id }}/tag/{{ $tag->id }}/attach"><span class="badge badge-{{ $tag->style }}">{{ $tag->name }}</span></a>
                        @endforeach
                    </p>
                    @endif

                </div>
            </div>
            <div class="mt-2">
                {{-- <a href="{{ route('hobby.index') }}" class="btn btn-primary"> --}}
                {{--
                <a href="{{ URL::previous() }}" class="btn btn-primary">
                    <i class="fas fa-arrow-circle-up"></i>
                    Back to overview
                </a>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
