@extends('layouts.app')

@section('page_title', 'Hobby List')
@section('page_description', '')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                @isset($filter)
                    <div class="card-header">Filtered Hobbies by <span style="font-size: 130%;" class="mx-2 badge badge-{{ $filter->style }}">{{ $filter->name }}</span>
                        <span class="float-right"><a href="/hobby">Show all hobbies</a></span>
                    </div>
                @else
                    <div class="card-header">All the Hobbies</div>
                @endisset


                <div class="card-body">
                    {{-- @dump($hobbies->toArray()) --}}
                    <ul class="list-group">
                        @foreach($hobbies as $hobby)
                            <li class="list-group-item">
                                <a href="{{ route('hobby.show', ['hobby' => $hobby]) }}" title="show details">{{ $hobby->name }}</a>
                                @auth
                                    <a href="{{ route('hobby.edit', ['hobby' => $hobby]) }}" class="btn btn-sm btn-light ml-3"><i class="fas fa-edit"></i> Edit Hobby</a>
                                @endauth
                                <span class="mx-2">Posted by:
                                    <a href="{{ route('user.show', ['user' => $hobby->user]) }}">
                                        {{ $hobby->user->name }}
                                    </a>
                                    ({{ $hobby->user->hobbies->count() }} hobbies)
                                </span>
                                @auth
                                    <form class="form-inline float-right" action="{{ route('hobby.destroy', compact('hobby')) }}" method="post">
                                        @csrf @method('DELETE')
                                        <input class="btn btn-sm btn-outline-danger" type="submit" value="Delete">
                                    </form>
                                @endauth
                                <span class="float-right mx-2">{{ $hobby->created_at->diffForHumans() }}</span>
                                <br>
                                @foreach($hobby->tags as $tag)
                                    <a href="{{ route('hobby_tag', ['tag_id' => $tag->id ]) }}"><span class="badge badge-{{ $tag->style }}">{{ $tag->name }}</span></a>
                                @endforeach
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="mt-3">
                {{ $hobbies->links() }}
            </div>
            @auth
                <div class="mt-2">
                    <a href="{{ route('hobby.create') }}" class="btn btn-success">
                        <i class="fas fa-plus-circle"></i>
                        Create new hobby
                    </a>
                </div>
            @endauth
        </div>
    </div>
</div>
@endsection
