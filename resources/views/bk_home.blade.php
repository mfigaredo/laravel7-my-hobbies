@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h2>Hello {{ auth()->user()->name }}</h2>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

{{--                    {{ __('You are logged in!') }}--}}
                    <a href="{{ route('hobby.create') }} " class="btn btn-success btn-sm">
                        <i class="fas fa-plus-circle"></i> Create new hobby
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
