@extends('layouts.app')

@section('page_title', 'About this project')
@section('page_description')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">INFO</div>

                <div class="card-body">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex corrupti deleniti nisi, animi tempora distinctio voluptatum voluptatem omnis. Corrupti, ipsam! Labore sunt iste, debitis repellat quas ipsam. Quaerat, doloremque. Quia a nobis, eveniet molestias odit commodi magni cupiditate ad neque. Dolorem impedit illum iste deserunt? Eius velit perferendis dolorum ut. Nostrum soluta quisquam perspiciatis aliquam. Dolore nemo eligendi provident voluptatem odit, ab, aliquam commodi, officia at omnis veniam. Iusto minus, corporis inventore architecto illum optio voluptate iste hic doloribus nesciunt explicabo fuga facere debitis molestiae id maxime quae commodi ad deserunt quam. Fugit aut ducimus adipisci labore ad esse quo.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
