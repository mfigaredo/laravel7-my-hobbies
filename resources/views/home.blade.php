@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <h2>Hello {{ auth()->user()->name }}</h2>
                            <h5>Your Motto</h5>
                            <p>{{ auth()->user()->motto ?? '' }}</p>
                            <h5>Your "About Me" -Text.</h5>
                            <p>{{ auth()->user()->about_me ?? '' }}</p>
                            <div class="my-3">
                                <a href="/user/{{ auth()->user()->id }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Edit Profile</a>
                            </div>
                        </div>
                        @if(file_exists(public_path() . '/img/users/' . auth()->user()->id . '_large.jpg'))
                            <div class="col-md-3">
                                <img class="img-thumbnail" src="/img/users/{{ auth()->user()->id }}_large.jpg" alt="{{ auth()->user()->name }}">
                            </div>
                        @endif
                    </div>



                    @isset($hobbies)
                    @if($hobbies->count() > 0)
                        <div class="col-12 d-flex justify-content-between align-items-center my-3">


                        <h3 class="row">Your Hobbies:</h3>
                        <a class="btn btn-success btn-sm" href="/hobby/create"><i class="fas fa-plus-circle"></i> Create new Hobby</a>
                        </div>
                    @endif
                    <ul class="list-group">
                        @foreach($hobbies as $hobby)
                            <li class="list-group-item">
                                @if(file_exists('img/hobbies/' . $hobby->id . '_thumb.jpg'))
                                    <a title="Show Details" href="/hobby/{{ $hobby->id }}">
                                        <img src="/img/hobbies/{{ $hobby->id }}_thumb.jpg" alt="Hobby Thumb">
                                    </a>&nbsp;
                                @endif

                                <a title="Show Details" href="/hobby/{{ $hobby->id }}">{{ $hobby->name }}</a>
                                @auth
                                    <a class="btn btn-sm btn-light ml-2" href="/hobby/{{ $hobby->id }}/edit"><i class="fas fa-edit"></i> Edit Hobby</a>
                                @endauth

                                @auth
                                    <form class="float-right" style="display: inline" action="/hobby/{{ $hobby->id }}" method="post">
                                        @csrf
                                        @method("DELETE")
                                        <input class="btn btn-sm btn-outline-danger" type="submit" value="Delete">
                                    </form>
                                @endauth
                                <span class="float-right mx-2">{{ $hobby->created_at->diffForHumans() }}</span>
                                <br>
                                @foreach($hobby->tags as $tag)
                                    <a href="/hobby/tag/{{ $tag->id }}"><span class="badge badge-{{ $tag->style }}">{{ $tag->name }}</span></a>
                                @endforeach
                            </li>
                        @endforeach
                    </ul>
                        <div class="py-3 d-flex justify-content-center">
                            {{ $hobbies->links() }}
                        </div>
                    @endisset

                    <div class="py-3 d-none">

                        <a class="btn btn-success btn-sm" href="/hobby/create"><i class="fas fa-plus-circle"></i> Create new Hobby</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
