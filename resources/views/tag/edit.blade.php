@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Tag</div>
                    <div class="card-body">
						<form action="{{ route('tag.update', compact('tag')) }}" method="post">
                            @csrf @method('PUT')
                            {{-- @php $btnText = 'Update Tag' @endphp --}}
                            @include('tag._form', ['btnText' => 'Update Tag'])
                        </form>
                        <a class="btn btn-primary float-right" href="{{ route('tag.index') }}"><i class="fas fa-arrow-circle-up"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection