@csrf
<div class="form-group">
	<label for="name">Name</label>
	<input type="text" class="form-control{{ $errors->has('name') ? ' border-danger': ''}}" id="name" name="name" value="{{ old('name', $tag->name) }}">
	<small class="form-text text-danger">{!! $errors->first('name') !!}</small>
</div>
<div class="form-group">
	<label for="style">Style</label>
	<input type="text" class="form-control{{ $errors->has('style') ? ' border-danger': ''}}" id="style" name="style" value="{{ old('style', $tag->style) }}">
	<small class="form-text text-danger">{!! $errors->first('style') !!}</small>
</div>
<input class="btn btn-primary mt-4" type="submit" value="{{ $btnText }}">