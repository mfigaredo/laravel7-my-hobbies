@extends('layouts.app')

@section('page_title', 'Tag List')
@section('page_description')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">All Tags</div>

                    <div class="card-body">
                        {{-- @dump($hobbies->toArray()) --}}
                        <ul class="list-group">
                            @foreach($tags as $tag)
                                <li class="list-group-item">
                                    {{-- <a href="{{ route('hobby.show', compact('tag')) }}" title="show details">{{ $tag->name }}</a> --}}
                                    {{-- {{ $tag->name }} --}}
                                    <span style="font-size: 130%;" class="mr-2 badge badge-{{ $tag->style }}">{{ $tag->name }}</span>

                                    <a href="{{ route('tag.edit', compact('tag')) }}" class="btn btn-sm btn-outline-primary ml-3"><i class="fas fa-edit"></i> Edit</a>
                                    <form class="form-inline float-right" action="{{ route('tag.destroy', compact('tag')) }}" method="post">
                                        @csrf @method('DELETE')
                                        <input class="btn btn-sm btn-outline-danger" type="submit" value="Delete">
                                    </form>
                                    <a href="/hobby/tag/{{ $tag->id  }}" class="float-right mx-2">Used {{ $tag->hobbies->count()  }} times</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="mt-2">
                    <a href="{{ route('tag.create') }}" class="btn btn-success">
                        <i class="fas fa-plus-circle"></i>
                        Create new tag
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
