@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit User</div>
                    <div class="card-body">
						<form action="/user/{{$user->id}}" method="post" enctype="multipart/form-data" autocomplete="off">
							@csrf @method('PUT')
                            <div class="form-group">
                                <label for="name">Name</label>
								<input type="text" class="form-control{{ $errors->has('name') ? ' border-danger': ''}}" id="name" name="name" value="{{  old('name', $user->name) }}">
								<small class="form-text text-danger">{!! $errors->first('name') !!}</small>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
								<input type="text" class="form-control{{ $errors->has('email') ? ' border-danger': ''}}" id="email" name="email" value="{{  old('name', $user->email) }}">
								<small class="form-text text-danger">{!! $errors->first('email') !!}</small>
                            </div>
                            <div class="form-group">
                                <label for="motto">Motto</label>
								<input type="text" class="form-control{{ $errors->has('motto') ? ' border-danger': ''}}" id="motto" name="motto" value="{{  old('name', $user->motto) }}">
								<small class="form-text text-danger">{!! $errors->first('motto') !!}</small>
                            </div>
                            @if( file_exists(public_path() . '/img/users/' . $user->id . '_large.jpg') )
                                <div class="mb-2 d-flex justify-content-between align-items-start">
                                    <img style="max-width: 400px; max-height:300px" src="/img/users/{{ $user->id }}_large.jpg" alt="">
                                    <a href="/delete-images/user/{{$user->id}}" class="mx-3 btn btn-outline-danger">Delete Image</a>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control{{ $errors->has('image') ? ' border-danger': ''}}" id="image" name="image" value="">
                                <small class="form-text text-danger">{!! $errors->first('image') !!}</small>
                            </div>
                            <div class="form-group">
                                <label for="about_me">About Me</label>
								<textarea class="form-control{{ $errors->has('about_me') ? ' border-danger': ''}}" id="about_me" name="about_me" rows="5">{{ old('about_me', $user->about_me) }}</textarea>
								<small class="form-text text-danger">{!! $errors->first('about_me') !!}</small>
                            </div>
                            <input class="btn btn-primary mt-4" type="submit" value="Update User">
                        </form>
                        <a class="btn btn-primary float-right" href="{{ route('user.index') }}"><i class="fas fa-arrow-circle-up"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
