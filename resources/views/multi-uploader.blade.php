@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Multi Uploader Vuejs Laravel</div>

                    <multi-uploader></multi-uploader>
                </div>
            </div>
        </div>
    </div>
@endsection
